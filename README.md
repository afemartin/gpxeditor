GpxEditor
===============================================================================

GpxEditor is a SPA built with NodeJs + Express with 2 flavours:

  * React.js + Redux
  * AngularJs 2

## Development environment requirements

  * Install [NodeJs + NPM](https://nodejs.org)
