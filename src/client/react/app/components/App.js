import React from 'react';
import Xml2Js from 'xml2js';

import Track from './Track';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {file: null, gpx: null};
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleParsedXml = this.handleParsedXml.bind(this);
    }

    handleFileUpload(event) {
        this.setState({file: event.target.files[0]});
        let reader = new FileReader();
        // when the file is finally loaded it is parsed by the Xml2Js library and converted into a js object
        reader.onload = event => Xml2Js.parseString(event.target.result, this.handleParsedXml);
        // when the file is read it triggers the "onload" event above
        reader.readAsText(event.target.files[0]);
    }

    handleParsedXml(error, result) {
        if (error) {
            this.setState({file: null});
            alert('The file uploaded is not a valid gpx format');
        } else {
            this.setState({gpx: result.gpx});
        }
    }

    render() {
        return (
            !this.state.file ?
            <form>
                <p>Please upload you .gpx file</p>
                <label className="custom-file" style={{top: "-5px", marginRight: "10px"}}>
                    <input type="file" className="custom-file-input" onChange={this.handleFileUpload}></input>
                    <span className="custom-file-control"></span>
                </label>
            </form>
            :
            this.state.gpx ?
            <div>
                { this.state.gpx.trk.map((track, index) =>
                    <Track key={index} data={track} />
                )}
            </div>
            :
            <p>Processing...</p>
        );
    }
}

export default App;
