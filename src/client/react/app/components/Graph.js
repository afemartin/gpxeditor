import React from 'react';

import Chart from 'chart.js';
import Moment from 'moment-timezone';

class Graph extends React.Component {

    constructor(props) {
        super(props);
        let time = [];
        let altitude = [];
        props.data.trkseg.map((segment) =>
            segment.trkpt.map((point) => {
                    time.push(Moment.tz(point.time[0], 'Asia/Tokyo'));
                    altitude.push(parseInt(point.ele[0]));
                }
            )
        );
        this.state = {time: time, altitude: altitude};
    }

    componentDidMount() {
        let ctx = document.getElementById("altitudeChart");
        let altitudeChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: this.state.time,
                datasets: [
                    {
                        label: "Altitude",
                        fill: false,
                        lineTension: 0.2,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        pointBorderWidth: 0,
                        pointHoverRadius: 0,
                        pointRadius: 0,
                        data: this.state.altitude
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'hour',
                            displayFormats: {
                                hour: 'hh:mm'
                            }
                        }
                    }]
                }
            }
        });
    }

    render() {
        return (
            <canvas id="altitudeChart" style={{marginTop: '20px', marginBottom: '20px'}}></canvas>
        );
    }
}

export default Graph;
