import React from 'react';

class Map extends React.Component {

    constructor(props) {
        super(props);
        let coordinates = [];
        props.data.trkseg.map((segment) =>
            segment.trkpt.map((point) =>
                coordinates.push({lat: parseFloat(point.$.lat), lng: parseFloat(point.$.lon)})
            )
        );
        this.state = {coordinates: coordinates};
    }

    componentDidMount() {
        let map = new google.maps.Map(document.getElementById('map'), {
            mapTypeId: 'terrain'
        });
        let path = new google.maps.Polyline({
            path: this.state.coordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 4
        });
        let markerStart = new google.maps.Marker({
            position: this.state.coordinates[0],
            map: map,
            icon: '/assets/img/bicycle-marker.png'
        });
        let markerFinish = new google.maps.Marker({
            position: this.state.coordinates[this.state.coordinates.length - 1],
            map: map,
            icon: '/assets/img/finish-flag-marker.png'
        });
        let bounds = new google.maps.LatLngBounds();
        for (let i = 0; i < this.state.coordinates.length; i++) {
            bounds.extend(this.state.coordinates[i]);
        }
        path.setMap(map);
        map.fitBounds(bounds);
    }

    render() {
        return (
            <div id="map" style={{height: '600px', marginTop: '20px', marginBottom: '20px'}}></div>
        );
    }
}

export default Map;