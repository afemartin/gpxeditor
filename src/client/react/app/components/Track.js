import React from 'react';

import Moment from 'moment-timezone';

import Map from "./Map";
import Graph from "./Graph";

class Track extends React.Component {

    constructor(props) {
        super(props);
        this.state = {data: props.data, edit: null};
        this.editTrackName = this.editTrackName.bind(this);
        this.saveTrackName = this.saveTrackName.bind(this);
    }

    editTrackName() {
        this.setState({edit: 'name'});
    }

    saveTrackName() {
        // This is not the right way to update the state but it works, probably because later the state is modified
        this.state.data.name = this.refs.inputName.value;
        // The approach bellow cannot be used since it will replace the previous data value
        // this.setState({data: {name: this.refs.inputName.value}});
        this.setState({edit: null});
    }

    render() {
        return (
            <div>
                {this.state.edit !== 'name' ?
                <div>
                    <h3 style={{display: 'inline'}}>{this.state.data.name} <button className="btn btn-sm" onClick={this.editTrackName}><i className="fa fa-pencil"></i></button></h3>
                </div>
                :
                <div className="input-group">
                    <input ref="inputName" type="text" className="form-control" defaultValue={this.state.data.name} />
                    <span className="input-group-btn">
                        <button className="btn btn-primary" type="button" onClick={this.saveTrackName}>Save</button>
                    </span>
                </div>
                }
                <Map data={this.state.data} />
                <Graph data={this.state.data} />
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>Segment</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                        <th>Altitude (meters)</th>
                        <th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.data.trkseg.map((segment, indexSegment) =>
                        segment.trkpt.map((point, indexPoint) =>
                            <tr key={indexSegment + '-' + indexPoint}>
                                <th>{indexSegment}</th>
                                <td>{point.$.lat}</td>
                                <td>{point.$.lon}</td>
                                <td>{point.ele[0]}</td>
                                <td>{Moment.tz(point.time[0], 'Asia/Tokyo').format('hh:mm:ss (z)')}</td>
                            </tr>
                        )
                    )}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Track;