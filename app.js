var express = require('express');
var app = express();

var config = require('./src/server/config');

app.set('view engine', 'jade');
app.set('views', './src/server/views');

app.get('/', function (req, res) {
    res.render('index');
});

app.get('/react', function (req, res) {
    res.render('react', {googleApiKey: config.google_api_key});
});

app.use('/build/react', express.static('src/client/react/build'));

app.get('/angular', function (req, res) {
    res.render('angular');
});

app.use('/build/angular', express.static('src/client/angular/build'));

app.use('/assets/img', express.static('src/server/assets/img'));

app.listen(8080, 'localhost', function () {
    console.log('GpxEditor app listening on port 8080!');
});
